'use strict';

import globalize from './internal/globalize';

/**
 * Finds the index of an element in the array.
 *
 * @param {Array} The array being searched.
 * @param {Mixed} item Element which will be searched for.
 * @param {Integer} fromIndex The index from which the item will be searched. Negative values will search from the end of the array.
 *
 * @returns {Integer}
 */
function indexOf (array, item, fromIndex) {
    var length = array.length;

    if (!fromIndex) {
        fromIndex = 0;
    } else if (fromIndex < 0) {
        fromIndex = Math.max(0, length + fromIndex);
    }

    for (var i = fromIndex; i < length; i++) {
        if (array[i] === item) {
            return i;
        }
    }

    return -1;
}

globalize('indexOf', indexOf);

export default indexOf;
