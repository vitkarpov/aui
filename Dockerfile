FROM atlassianlabs/docker-node-jdk-chrome-firefox:latest

WORKDIR /usr/src/app

ONBUILD COPY .npmrc /usr/src/app/.npmrc
ONBUILD COPY package.json /usr/src/app/package.json
ONBUILD RUN npm install --ignore-scripts
ONBUILD COPY . /usr/src/app
ONBUILD RUN npm run prepublish

EXPOSE 7000
