---
component: Single select
layout: main-layout.html
analytics:
  pageCategory: component
  component: single-select
  componentApiType: web-component
---

<a href="https://design.atlassian.com/latest/product/patterns/typeahead" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Description</h3>
<p>Renders a type-ahead where the user can select only one option.  The suggestions will match what the user types in the input field.  </p>

<h3>Summary</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><span class="aui-lozenge aui-lozenge-current">experimental</span></td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web resource key.</td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:dialog2"><code>com.atlassian.auiplugin:aui-select</code></td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>5.8.0</td>
        </tr>
    </tbody>
</table>

<h3>Examples</h3>
<p>Single Select is a purely markup initialised component, these code samples correspond to the examples above.</p>

<h4>Synchronous (HTML)</h4>
<form class="aui">
    <aui-label for="sync-example-product-single-select">Choose your product synchronously:</aui-label>
    <p>
        <aui-select name="product" id="sync-example-product-single-select" placeholder="Select a product">
            <aui-option>HipChat</aui-option>
            <aui-option>JIRA</aui-option>
            <aui-option>Confluence</aui-option>
            <aui-option>Stash</aui-option>
            <aui-option>FeCru</aui-option>
        </aui-select>
    </p>
</form>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <form class="aui">
            <aui-label for="sync-product-single-select">Choose your product synchronously:</aui-label>
            <p>
                <aui-select
                    id="sync-product-single-select"
                    name="product"
                    placeholder="Select a product"
                >
                    <aui-option>HipChat</aui-option>
                    <aui-option>JIRA</aui-option>
                    <aui-option>Confluence</aui-option>
                    <aui-option>Stash</aui-option>
                    <aui-option>FeCru</aui-option>
                </aui-select>
            </p>
        </form>
    </noscript>
</aui-docs-example>

<h4>Asynchronous (HTML)</h4>
<form class="aui">
    <aui-label for="async-example-product-single-select">Choose your product asynchronously (you need to type to initiate a search):</aui-label>
    <p>
        <aui-select name="product" id="async-example-product-single-select" src="products" value="type to trigger async" placeholder="Select a product"></aui-select>
    </p>
</form>

<script>
    var server = sinon.fakeServer.create();
    server.autoRespond = true;
    server.autoRespondAfter = 1000;
    server.respondWith(/products/, [
        200,
        { 'Content-Type': 'application/json' },
        '[{ "label": "HipChat" }, { "label": "JIRA" }, { "label": "Confluence", "value": "CONF" }, { "label": "Stash", "value": "STASH" }, { "label": "FeCru", "value": "FEC" }]'
    ]);
</script>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <form class="aui">
            <aui-label for="async-product-single-select">Choose your product asynchronously (you need to type to initiate a search):</aui-label>
            <p>
                <aui-select
                    id="async-product-single-select"
                    name="product"
                    placeholder="Select a product"
                    src="products"
                    value="type to trigger async"
                ></aui-select>
            </p>
        </form>
    </noscript>
</aui-docs-example>


<h3>Initialization</h3>

<p>There is no need to call any javascript to initialize an aui select component, you simply need to add the <aui-docs-component>aui-select</aui-docs-component> element with your desired options on the page.</p>

<h3>HTML API</h3>
<h4>aui-select</h4>
These options are passed in as attributes on the <aui-docs-component>aui-select</aui-docs-component> element.
<table class="aui api-table">
    <thead>
        <th>Attribute</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>id</td>
            <td>
                <p>The id to be placed on the input element of the select this id is removed from the aui-select element after initialization</p>
                <p>This id is used for accessibility purposes; behavior/styling should not be bound to it as this could change in the future.</p>
            </td>
        </tr>
        <tr>
            <td>name</td>
            <td>The name attribute that is to be put on the input element</td>
        </tr>
        <tr>
            <td>placeholder</td>
            <td>The placeholder value that the input should use.</td>
        </tr>
        <tr>
            <td>src</td>
            <td>
                <p>The URL that is to be used for an async single-select. The expected response format from the server is a JSON array of objects with label and value properties:</p>
                <noscript is="aui-docs-code" type="text/js">
                    [
                      {"label": "First Value"},
                      {"label": "Second Value"},
                      {"label": "Third Value", "value": "third-value"},
                      {"label": "Fourth Value", "value": "fourth-value", "img-src": "url/avatar.png"}
                    ]
                </noscript>
                <p>The URL should return the full set of results that requires client-side filtering.</p>
                <strong>Notes for server implementations</strong>
                <p>For large datasets it might be necessary to do some server-side filtering as well. The AUI single select makes a request with a <code>q</code> query parameter which is taken from the value of the input field; if you wish to filter server-side you simply need to accept this parameter and do filtering based on its value. The parameter used is not configurable. Sample URL:</p>
                <noscript is="aui-docs-code" type="text/html">
                    http://my-server.com/searchResults?q=$valueFromInput
                </noscript>
                <p>In this example the <code>src</code> attribute would be set to <code>http://my-server.com/searchResults</code> or <code>/searchResults</code></p>
            </td>
        </tr>
        <tr>
            <td>no-empty-value</td>
            <td>A boolean attribute specifying that empty values are not allowed for this field.</td>
        </tr>
        <tr>
            <td>can-create-values</td>
            <td>A boolean attribute that allows a user of the <code>&lt;aui-select&gt;</code> to select any value, rather than just those in the option list or remote source.</td>
        </tr>
    </tbody>
</table>

<h4>aui-option</h4>
<p>The options placed inside the select also have some attribute options. To set the display value of the option simply set the text node within the <code>aui-option</code> element.</p>
<table class="aui api-table">
    <thead>
        <th>Attribute</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>value</td>
            <td>The value of the option used by the select to determine the value of the input field when it is selected</td>
        </tr>
        <tr>
            <td>img-src</td>
            <td>A url to an image, can be a relative URL</td>
        </tr>
        <tr>
            <td>selected</td>
            <td>A boolean attribute, whether the current option is the selected one. Implicitly controls the <code>value</code> attribute of the containing <aui-docs-component>aui-select</aui-docs-component>. First <aui-docs-component>aui-option</aui-docs-component> with the attribute wins.</td>
        </tr>
    </tbody>
</table>

<h3>JavaScript API</h3>
<h4> aui-select </h4>
<p> These javascript methods and properties for the aui-select are found on the parent element of the select.</p>
<p> To find them simply use native DOM methods: </p>
<noscript is="aui-docs-code" type="text/js">
    document.getElementById("my-aui-select-element");
</noscript>

<table class="aui api-table">
    <thead>
        <th>Method/Property</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>value</td>
            <td>Can be used to get and set the current selected value of the select.</td>
        </tr>
    </tbody>
</table>

<h4>aui-option</h4>
<p> These javascript methods and properties for the aui-option are found on the option element itself.</p>
<p> To find them simply use native DOM methods: </p>
<noscript is="aui-docs-code" type="text/js">
    document.querySelector('#my-aui-select aui-option[value="my-value"]');
</noscript>

<table class="aui api-table">
    <thead>
        <th>Method/Property</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>value</td>
            <td>Can be used to get and set the associated value of the option.</td>
        </tr>
        <tr>
            <td>serialize</td>
            <td>Turns the value, label and image properties of the aui-option into a JSON object.</td>
        </tr>
    </tbody>
</table>

<h3>Events</h3>
<table class="aui api-table">
    <thead>
        <th>Event name</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>change</td>
            <td>is triggered after the single select value is modified either by selection or by setting the value through javascript.</td>
        </tr>
    </tbody>
</table>
