/* eslint-disable max-len */
/*eslint no-trailing-spaces:0*/
export default `
        <div id="page">
            <header id="header" role="banner">
                <nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
                    <div class="aui-header-inner">
                        <div class="aui-header-primary">
                            HEADER
                        </div>
                    </div>
                    <!-- .aui-header-inner-->
                </nav>
                <!-- .aui-header -->
            </header>
            <!-- #header -->
            <section id="content" role="main">
                <div class="aui-sidebar " >
                    <div class="aui-sidebar-wrapper">
                        <div class="aui-sidebar-body">
                            <header class="aui-page-header">
                                <div class="aui-page-header-inner">
                                    <div class="aui-page-header-image" id="test-sidebar-avatar"><span class="aui-avatar aui-avatar-large aui-avatar-project"><span class="aui-avatar-inner"></span></span></div>
                                    <!-- .aui-page-header-image -->
                                    <div class="aui-page-header-main">
                                        <h1>Sidebar Page Layout</h1>
                                        <ol class="aui-nav aui-nav-breadcrumbs">
                                            <li><a href="home.html"><span class="aui-nav-item-label">Breadcrumbs or subtitle</span></a></li>
                                        </ol>
                                    </div>
                                    <!-- .aui-page-header-main -->
                                </div>
                                <!-- .aui-page-header-inner -->
                            </header>
                            <!-- .aui-page-header -->
                            <nav class="aui-navgroup aui-navgroup-vertical">
                                <div class="aui-navgroup-inner">
                                    <div class="aui-sidebar-group aui-sidebar-group-actions" id="test-expandable-submenu-trigger">
                                        <div id="test-sidebar-page-actions" class="aui-nav-heading" title="Stuff"><strong>Page Actions</strong></div>
                                        <ul class="aui-nav aui-expander-content" title="Page actions" id="actions" resolved aria-expanded="false">
                                            <li><a href="#" title="Create" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">Create</span></a></li>
                                            <li><a href="#" title="Edit" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">Edit</span></a></li>
                                            <li><a href="#" title="View" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">View</span></a></li>
                                            <li class="aui-expander-cutoff"><a href="#" title="More" class="aui-nav-item aui-expander-trigger" aria-controls="actions" aria-expanded="false"><span class="aui-icon aui-icon-small"></span><span class="aui-nav-item-label">More…</span></a></li>
                                            <li><a href="#" title="Delete" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">Delete</span></a></li>
                                            <li><a href="#" title="Rename" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">Rename</span></a></li>
                                        </ul>
                                    </div>
                                    <div class="aui-sidebar-group" id="test-no-submenu-trigger">
                                        <div class="aui-nav-heading" title="Stuff"><strong>Empty section</strong></div>
                                    </div>
                                    <div class="aui-sidebar-group" id="test-submenu-trigger">
                                        <div class="aui-nav-heading" title="Stuff"><strong>Page Layouts</strong></div>
                                        <ul class="aui-nav">
                                            <li><a href="index.soy" class="aui-nav-item"><span class="aui-nav-item-label">Default/fluid</span></a></li>
                                            <li><a href="focused.soy" class="aui-nav-item"><span class="aui-nav-item-label">Focused/task page</span></a></li>
                                            <li><a href="fixed.soy" class="aui-nav-item"><span class="aui-nav-item-label">Fixed width page</span></a></li>
                                            <li><a href="hybrid.soy" class="aui-nav-item"><span class="aui-nav-item-label">Hybrid page</span></a></li>
                                            <li><a href="sidebar.soy" class="aui-nav-item"><span class="aui-nav-item-label">Sidebar page</span></a></li>
                                            <li class="aui-nav-selected"><a href="sidebarWithPageHeader.soy" class="aui-nav-item"><span class="aui-nav-item-label">Sidebar page with Page Header</span></a></li>
                                            <li><a href="sidebarWithPageHeaderAndNav.soy" class="aui-nav-item"><span class="aui-nav-item-label">Sidebar page with Header and Nav</span></a></li>
                                        </ul>
                                    </div>
                                    <div class="aui-sidebar-group aui-sidebar-group-tier-one">
                                        <div class="aui-nav-heading" title="Stuff"><strong>Simple Nav</strong></div>
                                        <ul class="aui-nav">
                                            <li><a href="board-1.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-info"></span><span class="aui-nav-item-label">Summary</span></a><a href="#" class="aui-nav-item-actions">More Actions</a></li>
                                            <li aria-expanded="true">
                                                <a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a><a href="#" class="aui-nav-item"><span class="aui-badge">63</span><span class="aui-icon aui-icon-small aui-iconfont-details"></span><span class="aui-nav-item-label">Issues</span></a>
                                                <ul class="aui-nav" title="one">
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">Triage</span></a></li>
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">Open</span></a></li>
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">In Progress</span></a></li>
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">Code Review</span></a></li>
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">QA</span></a></li>
                                                    <li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">Done</span></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="8_inline_add_2.html" class="aui-nav-item" title="This is a really really long title for whatever this is"><span class="aui-icon aui-icon-small aui-iconfont-jira-completed-task"></span><span class="aui-nav-item-label">Road Map</span></a><a href="#" class="aui-nav-item-actions">More Actions</a></li>
                                            <li><a href="reports.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-devtools-side-diff"></span><span class="aui-nav-item-label">Change Log</span></a></li>
                                            <li><a href="reports.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-details"></span><span class="aui-nav-item-label">Popular Issues</span></a></li>
                                            <li><a href="home.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">Versions</span></a></li>
                                            <li><a href="home.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-devtools-submodule"></span><span class="aui-nav-item-label">Components</span></a></li>
                                            <li><a href="home.html" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-devtools-tag"></span><span class="aui-nav-item-label">Labels</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="aui-sidebar-footer"><a href="http://example.com/" class="aui-button aui-button-subtle aui-sidebar-settings-button" data-tooltip="Settings"><span class="aui-icon aui-icon-small aui-iconfont-configure"></span><span class="aui-button-label">Settings</span></a><a class="aui-button aui-button-subtle aui-sidebar-toggle aui-sidebar-footer-tipsy" data-tooltip="Expand sidebar ( [ )" href="#"><span class="aui-icon aui-icon-small"></span></a></div>
                    </div>
                </div>
                <header class="aui-page-header">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1>Settings</h1>
                        </div>
                        <!-- .aui-page-header-main -->
                    </div>
                    <!-- .aui-page-header-inner -->
                </header>
                <!-- .aui-page-header -->
                <div class="aui-page-panel">
                    <div class="aui-page-panel-inner">
                        <section class="aui-page-panel-content">
                            <h2>Page content heading</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae diam in arcu ultricies gravida sed sed nisl. Curabitur nibh nulla, semper non pharetra eu, suscipit vitae eros. Donec eget lectus elit. Etiam metus diam, adipiscing convallis blandit sit amet, sollicitudin sit amet felis. Phasellus justo elit, rhoncus sed tincidunt a, auctor sit amet turpis. Praesent turpis lectus, aliquet vitae sollicitudin ac, convallis vitae urna. Donec consectetur lacus a lacus tincidunt at varius felis venenatis. Pellentesque dapibus mattis arcu, a vestibulum lacus congue at.</p>
                            <blockquote>
                                <p>All that is gold does not glitter, not all those who wander are lost; The old that is strong does not wither, deep roots are not reached by the frost.</p>
                                <p> From the ashes a fire shall be woken, a light from the shadows shall spring; Renewed shall be blade that was broken, the crownless again shall be king.</p>
                                <cite>J.R.R. Tolkien, The Fellowship of the Ring</cite>
                            </blockquote>
                            <p>Integer nunc nisi, condimentum venenatis euismod et, pulvinar vel sem. Fusce semper ipsum eget libero aliquam cursus et et nisl. Nunc at felis odio. Suspendisse ante urna, eleifend ac pellentesque et, dignissim sit amet eros. Sed varius egestas cursus. Suspendisse id orci nunc. Morbi feugiat, libero vitae pulvinar eleifend, nunc erat lobortis arcu, ac semper libero sem eget sapien. Etiam sit amet rhoncus nunc. Mauris commodo dictum elit in rutrum. Aenean bibendum, purus sit amet molestie mollis, nulla felis varius odio, sed egestas ipsum metus non ante. Aliquam erat volutpat. Donec eu risus justo. Suspendisse id lacus tellus, sed fringilla dolor. In consectetur pellentesque tristique.</p>
                            <h2>Why use subheadings?</h2>
                            <p>Phasellus quis nunc ac magna aliquam euismod. Donec hendrerit libero id purus convallis feugiat. Mauris aliquet ullamcorper elit, a bibendum nunc euismod vitae. Nulla sollicitudin posuere nisi eu pretium. Vestibulum purus nibh, facilisis ac blandit vitae, consectetur a odio. Pellentesque ac interdum metus. Nulla pellentesque, arcu vel gravida dictum, ipsum enim viverra diam, vitae tincidunt felis enim quis metus.</p>
                            <ul>
                                <li>Lists can be used to split up large blocks of text</li>
                                <li>The spacing and indentation helps scanability</li>
                                <li>Just be sure you use the right type of list</li>
                            </ul>
                            <p>Sed quis pretium est. Sed pretium sollicitudin massa sit amet ultrices. Morbi lorem mi, varius ac porttitor non, feugiat id mauris. Sed elementum elementum elit, sed auctor turpis sagittis vel. Mauris fringilla tincidunt nibh, et posuere nisi fringilla eu. Duis adipiscing, mi at aliquam dapibus, velit libero dictum sapien, non vestibulum nisi dolor ut sem. Maecenas vehicula porttitor tellus in ornare. Praesent sit amet venenatis augue. Aliquam et lorem eget ipsum hendrerit gravida in quis elit. In hac habitasse platea dictumst.</p>
                            <h3>Multiple levels of headings are very useful</h3>
                            <p>Praesent nulla erat, elementum ut varius at, adipiscing id augue. Sed sagittis consectetur tempus. Nullam tristique feugiat eros, sit amet dapibus justo gravida quis. Praesent ante felis, pulvinar et convallis vel, viverra ac neque. Fusce tincidunt bibendum est eu venenatis. Sed tortor mauris, feugiat pharetra eleifend a, molestie sed lacus. Morbi nec sem ut libero mattis consequat. Praesent eget tellus turpis. Sed rutrum, sapien non tempor venenatis, enim augue tincidunt eros, quis hendrerit lectus libero nec dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis sit amet venenatis ante. Etiam leo est, malesuada at aliquet et, cursus in massa.</p>
                            <h4>Multiple levels of headings are very useful</h4>
                            <p>Praesent nulla erat, elementum ut varius at, adipiscing id augue. Sed sagittis consectetur tempus. Nullam tristique feugiat eros, sit amet dapibus justo gravida quis. Praesent ante felis, pulvinar et convallis vel, viverra ac neque. Fusce tincidunt bibendum est eu venenatis. Sed tortor mauris, feugiat pharetra eleifend a, molestie sed lacus. Morbi nec sem ut libero mattis consequat. Praesent eget tellus turpis. Sed rutrum, sapien non tempor venenatis, enim augue tincidunt eros, quis hendrerit lectus libero nec dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis sit amet venenatis ante. Etiam leo est, malesuada at aliquet et, cursus in massa.</p>
                            <h5>Multiple levels of headings are very useful</h5>
                            <p>Praesent nulla erat, elementum ut varius at, adipiscing id augue. Sed sagittis consectetur tempus. Nullam tristique feugiat eros, sit amet dapibus justo gravida quis. Praesent ante felis, pulvinar et convallis vel, viverra ac neque. Fusce tincidunt bibendum est eu venenatis. Sed tortor mauris, feugiat pharetra eleifend a, molestie sed lacus. Morbi nec sem ut libero mattis consequat. Praesent eget tellus turpis. Sed rutrum, sapien non tempor venenatis, enim augue tincidunt eros, quis hendrerit lectus libero nec dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis sit amet venenatis ante. Etiam leo est, malesuada at aliquet et, cursus in massa.</p>
                            <h6>Multiple levels of headings are very useful</h6>
                            <p>Praesent nulla erat, elementum ut varius at, adipiscing id augue. Sed sagittis consectetur tempus. Nullam tristique feugiat eros, sit amet dapibus justo gravida quis. Praesent ante felis, pulvinar et convallis vel, viverra ac neque. Fusce tincidunt bibendum est eu venenatis. Sed tortor mauris, feugiat pharetra eleifend a, molestie sed lacus. Morbi nec sem ut libero mattis consequat. Praesent eget tellus turpis. Sed rutrum, sapien non tempor venenatis, enim augue tincidunt eros, quis hendrerit lectus libero nec dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis sit amet venenatis ante. Etiam leo est, malesuada at aliquet et, cursus in massa.</p>
                        </section>
                        <!-- .aui-page-panel-content -->
                    </div>
                    <!-- .aui-page-panel-inner -->
                </div>
                <!-- .aui-page-panel -->
            </section>
            <!-- #content -->
            <footer id="footer" role="contentinfo">
                <section class="footer-body">
                    <ul>
                        <li>I &hearts; AUI</li>
                    </ul>
                    <div id="footer-logo"><a href="http://www.atlassian.com/">Atlassian</a></div>
                </section>
            </footer>
            <!-- #footer -->
        </div>
`;
/* eslint-enable max-len */
