'use strict';

import toInit from '../../../src/js/aui/to-init';

describe('aui/to-init', function () {
    it('globals', function () {
        expect(AJS.toInit).to.equal(toInit);
    });

    it('on multiple functions', function () {
        var func1 = sinon.spy();
        var func2 = sinon.spy();

        toInit(func1);
        toInit(func2);

        func1.should.have.been.calledOnce;
        func2.should.have.been.calledOnce;
    });

    it('should properly throw errors from passed in functions', function () {
        var error = new Error('test');
        var func1 = function() {
            throw error;
        };
        var func2 = sinon.spy();

        toInit(func1);
        toInit(func2);

        func1.should.throw(error);
        func2.should.have.been.calledOnce;
    });
});
