var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.series(
    gat.load('soy'),
    gulp.parallel(
        gat.load('dist/js'),
        gat.load('dist/less'),
        gat.load('dist/license')
    )
);
