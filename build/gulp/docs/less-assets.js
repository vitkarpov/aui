'use strict';

var gulp = require('gulp');
var rootPaths = require('../../lib/root-paths');

module.exports = gulp.parallel(
    function assets () {
        return gulp.src(rootPaths('docs/{*,**/*}'))
            .pipe(gulp.dest('.tmp/docs'));
    },
    function fontsAndImages () {
        return gulp.src(rootPaths('src/less/{fonts,images}/{*,**/*}'))
            .pipe(gulp.dest('.tmp/docs/src/styles'));
    }
);
