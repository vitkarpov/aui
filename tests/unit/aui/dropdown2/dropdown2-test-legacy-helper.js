import skate from 'skatejs';
import '../../../../src/js/aui/dropdown2';

var template = function (idPrefix) {
    return {
        plainSection:
            '<div id="' + idPrefix + 'section1" class="aui-dropdown2-section">' +
            '<ul>' +
            '<li><a href="#link" id="' + idPrefix + 'item1">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item2">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item3">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item4">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        plainSection2:
            '<div id="' + idPrefix + 'section2" class="aui-dropdown2-section">' +
            '<ul>' +
            '<li><a id="' + idPrefix + 'item12">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item22">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item32">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'item42">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        hiddenSection:
            '<div id="' + idPrefix + 'section3" class="aui-dropdown2-section">' +
            '<ul>' +
            '<li class="aui-dropdown2-hidden"><a id="' + idPrefix + 'hidden1-unchecked-disabled" class="aui-dropdown2-checkbox interactive disabled" >Menu item</a></li>' +
            '<li class="aui-dropdown2-hidden"><a id="' + idPrefix + 'hidden2-checked" class="aui-dropdown2-checkbox interactive aui-dropdown2-checked">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        interactiveSection:
            '<div id="' + idPrefix + 'section4" class="aui-dropdown2-section">' +
            '<ul role="radiogroup">' +
            '<li><a id="' + idPrefix + 'iradio1-interactive-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'iradio2-interactive-unchecked" class="aui-dropdown2-radio interactive">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'iradio3-unchecked" class="aui-dropdown2-radio">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        radioSection:
            '<div id="' + idPrefix + 'section2" class="aui-dropdown2-section">' +
            '<ul role="radiogroup">' +
            '<li><a id="' + idPrefix + 'radio1-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'radio2-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked aui-dropdown2-checked">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'radio3-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        checkboxSection:
            '<div id="' + idPrefix + '" class="aui-dropdown2-section">' +
            '<ul>' +
            '<li><a id="' + idPrefix + 'check1-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'check2-checked" class="aui-dropdown2-checkbox interactive checked aui-dropdown2-checked">Menu item</a></li>' +
            '<li><a id="' + idPrefix + 'check3-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
            '</ul>' +
            '</div>',

        submenuSection:
            '       <div class="aui-dropdown2-section">' +
            '           <ul class="aui-list-truncate">' +
            '               <li>' +
            '                   <a id="' + idPrefix + 'dd2-menu-1-child-1">Dummy item 1</a>' +
            '               </li>' +
            '               <li>' +
            '                   <a id="' + idPrefix + 'dd2-menu-1-child-2" href="#" aria-haspopup="true" class="interactive aui-dropdown2-sub-trigger aui-style-default">Open submenu level 1</a>' +
            '               </li>' +
            '           </ul>' +
            '       </div>'
    };
};

var LegacyDropdown = function () {
    var idPrefix = AJS.id('test-legacy-dropdown');
    var $dropdown = AJS.$('<div class="aui-dropdown2 aui-style-default" id="' + idPrefix + '-menu"></div>');

    var testComponent = {
        getItem: function (index, section) {
            var $itemContainer = section ? this.$dropdown.find('.aui-dropdown2-section').eq(section - 1) : this.$dropdown;
            return $itemContainer.find('li span, li a').eq(index - 1);
        },

        $dropdown: $dropdown,

        addTrigger: function () {
            this.$trigger = AJS.$('<a id="' + idPrefix + '-trigger" href="#' + idPrefix + '-menu" aria-owns="' + idPrefix + '-menu" aria-haspopup="true" class="aui-dropdown2-trigger">Example dropdown</a>');
        },

        addSecondTrigger: function () {
            this.$secondTrigger = AJS.$('<a id="' + idPrefix + '-second-trigger" href="#' + idPrefix + '-menu" aria-owns="' + idPrefix + '-menu" aria-haspopup="true" class="aui-dropdown2-trigger">Example dropdown(second trigger)</a>');
        },

        initialise: function ($parent) {
            this.$dropdown.appendTo($parent || AJS.$('#test-fixture'));
            skate.init(this.$dropdown[0]);

            if (this.$secondTrigger && this.$secondTrigger.length) {
                this.$secondTrigger.appendTo($parent || AJS.$('#test-fixture'));
                skate.init(this.$secondTrigger[0]);
            }
            if (this.$trigger && this.$trigger.length) {
                this.$trigger.appendTo($parent || AJS.$('#test-fixture'));
                skate.init(this.$trigger[0]);
            }
        },

        addPlainSection: function () {
            $('#' + idPrefix + 'section1').remove();
            $dropdown.append(
                template(idPrefix).plainSection
            );
        },

        addPlainSection2: function () {
            $('#' + idPrefix + 'section2').remove();
            $dropdown.append(
                template(idPrefix).plainSection2
            );
        },

        addHiddenSection: function () {
            $('#' + idPrefix + 'section3').remove();
            $dropdown.append(
                template(idPrefix).hiddenSection
            );
        },

        addInteractiveSection: function () {
            $('#' + idPrefix + 'section4').remove();
            $dropdown.append(
                template(idPrefix).interactiveSection
            );
        },

        addRadioSection: function () {
            $('#' + idPrefix + 'section2').remove();
            $dropdown.append(
                template(idPrefix).radioSection
            );
        },

        addCheckboxSection: function () {
            var id = idPrefix + '-checkbox-section';
            $('#' + id).remove();
            $dropdown.append(
                template(idPrefix).checkboxSection
            );
        },

        addSubmenuSection: function (dropdownToAssociate) {
            var id = idPrefix + '-submenu-section';
            $('#' + id).remove();

            var submenuId = dropdownToAssociate.$dropdown[0].id;
            var $submenu = $(template(idPrefix).submenuSection);
            var $submenuTrigger = $submenu.find('[aria-haspopup]');
            $submenuTrigger.attr('aria-controls', submenuId);

            $dropdown.append($submenu);

        },

        checkEvent: 'aui-dropdown2-item-check',
        uncheckEvent: 'aui-dropdown2-item-uncheck',

        isChecked: function (el) {
            return $(el).is('[aria-checked="true"].checked.aui-dropdown2-checked');
        },

        isUnchecked: function (el) {
            var $el = $(el);
            return !$el.is('.checked, .aui-dropdown2-checked') && $el.is('[aria-checked="false"]');
        }
    };

    return testComponent;
};

export default LegacyDropdown;
