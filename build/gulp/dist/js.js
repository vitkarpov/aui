var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpConcat = require('gulp-concat');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var gulpRename = require('gulp-rename');
var header = require('../../lib/header');
var isEs6 = require('../../lib/is-es6');
var lazyPipe = require('lazypipe');
var libTraceMap = require('../../lib/trace-map');
var libVersionReplace = require('../../lib/version-replace');
var gulpUglify = require('gulp-uglify');
var minimatch = require('minimatch');

var opts = gat.opts();

module.exports = function distJs () {
    var shouldMinify = !opts['no-minify'];

    var babelify = lazyPipe()
        .pipe(galv.cache, 'babel', gulpBabel());

    var minifyJs = lazyPipe()
        .pipe(gulpDebug, {title: 'write + optimise'})
        .pipe(gulpUglify)
        .pipe(gulpRename, {suffix: '.min'})
        .pipe(gulp.dest, 'dist/aui/js')
        .pipe(gulp.dest, '.tmp/dist/aui/js');

    var writeBundles = lazyPipe()
        .pipe(gulpDebug, {title: 'write'})
        .pipe(gulp.dest, 'dist/aui/js')
        .pipe(gulp.dest, '.tmp/dist/aui/js')
        .pipe(gulpIf, shouldMinify, minifyJs());

    var traced = galv.trace('src/js/aui*.js', {
        map: libTraceMap(opts.root)
    });

    return traced.createStream()
        // ES6 -> ES5 (globals)
        .pipe(libVersionReplace())
        .pipe(gulpIf(isEs6, babelify()))
        .pipe(galv.cache('globalize', galv.globalize()))

        // aui-css-deprecations.js
        .pipe(traced.filter({unique: true, origin: '**/src/js/aui-css-deprecations.js'}))
        .pipe(gulpDebug({title: '-> aui-css-deprecations.js'}))
        .pipe(gulpConcat('aui-css-deprecations.js'))
        .pipe(traced.restore())

        // aui-datepicker.js
        .pipe(traced.filter({unique: true, origin: '**/src/js/aui-datepicker.js'}))
        .pipe(gulpDebug({title: '-> aui-datepicker.js'}))
        .pipe(gulpConcat('aui-datepicker.js'))
        .pipe(traced.restore())

        // aui-header-async.js
        .pipe(traced.filter({unique: true, origin: '**/src/js/aui-header-async.js'}))
        .pipe(gulpDebug({title: '-> aui-header-async.js'}))
        .pipe(gulpConcat('aui-header-async.js'))
        .pipe(traced.restore())

        // aui-experimental.js
        .pipe(traced.filter({unique: true, origin: '**/src/js/aui-experimental.js'}))
        .pipe(gulpDebug({title: '-> aui-experimental.js'}))
        .pipe(gulpConcat('aui-experimental.js'))
        .pipe(traced.restore())

        // aui-soy.js
        .pipe(traced.filter({unique: true, origin: '**/src/js/aui-soy.js'}))
        .pipe(gulpDebug({title: '-> aui-soy.js'}))
        .pipe(gulpConcat('aui-soy.js'))
        .pipe(traced.restore())

        // aui.js
        .pipe(traced.filter({common: true}, {unique: true, origin: '**/src/js/aui.js'}))
        .pipe(gulpDebug({title: '-> aui.js'}))
        .pipe(gulpConcat('aui.js'))
        .pipe(traced.restore())

        .pipe(header())

        // Only write bundle files.
        .pipe(gulpIf(function (file) {
            return minimatch(file.path, '**/aui*.js');
        }, writeBundles()));
};
