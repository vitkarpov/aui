'use strict';

import alphanum from '../../../src/js/aui/alphanum';

describe('aui/alphanum', function () {
    it('globals', function () {
        expect(AJS.alphanum).to.equal(alphanum);
    });

    it('API', function () {
        function assertAlphaNum(a, b, expected) {
            var actual = AJS.alphanum(a, b);
            expect(actual).to.equal(expected);

            // try in reverse
            actual = AJS.alphanum(b, a);
            expect(actual).to.equal(expected * -1);
        }

        assertAlphaNum('a', 'a', 0);
        assertAlphaNum('a', 'b', -1);
        assertAlphaNum('b', 'a', 1);

        assertAlphaNum('a0', 'a1', -1);
        assertAlphaNum('a10', 'a1', 1);
        assertAlphaNum('a2', 'a1', 1);
        assertAlphaNum('a2', 'a10', -1);
    });
});
