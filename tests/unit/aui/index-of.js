'use strict';

import indexOf from '../../../src/js/aui/index-of';

describe('aui/index-of', function () {
    it('globals', function () {
        expect(AJS.indexOf).to.equal(indexOf);
    });

    describe('API', function () {
        it('an arbitrary array', function () {
            var arr = ['foo', 'bar', 'baz'];
            expect(indexOf(arr, 'foo')).to.equal(0);
            expect(indexOf(arr, 'baz')).to.equal(2);
            expect(indexOf(arr, 'blops')).to.equal(-1);
        });

        it('given a starting index', function () {
            var arr = ['foo', 'bar', 'baz'];
            expect(indexOf(arr, 'foo', 1)).to.equal(-1);
            expect(indexOf(arr, 'baz', 1)).to.equal(2);
            expect(indexOf(arr, 'blops', 1)).to.equal(-1);
        });

        it('given a negative starting index', function () {
            var arr = ['foo', 'bar', 'baz'];
            expect(indexOf(arr, 'foo', -1)).to.equal(-1);
            expect(indexOf(arr, 'baz', -1)).to.equal(2);
            expect(indexOf(arr, 'blops', -1)).to.equal(-1);
        });
    });
});
