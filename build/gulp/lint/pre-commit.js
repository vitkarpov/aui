'use strict';

var gulp = require('gulp');
var eslint = require('gulp-eslint');
var jscs = require('gulp-jscs');
var opts = require('gulp-auto-task').opts();
var stylish = require('gulp-jscs-stylish');

module.exports = function () {
    return gulp.src(opts.files.split(','))
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(jscs())
        .on('error', function () { /* noop */ })
        .pipe(stylish())
        .pipe(eslint.failAfterError());
};
