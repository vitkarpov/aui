'use strict';

import tablessortable from '../../../src/js/aui/tables-sortable';

describe('aui/tables-sortable', function () {
    it('globals', function () {
        expect(AJS.tablessortable).to.equal(tablessortable);
    });
});
