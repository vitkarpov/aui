'use strict';

import $ from '../../../src/js/aui/jquery';
import tabs from '../../../src/js/aui/tabs';

describe('aui/tabs-responsive', function () {
    var responsiveTabs = '<div class="aui-tabs horizontal-tabs" id="tabs-container" data-aui-persist="true" data-aui-responsive="true">' +
        '    <ul class="tabs-menu">' +
        '       <li class="menu-item active-tab">' +
        '           <a href="#horizontal-wrap-first">Tab 1 - Active</a>' +
        '       </li>' +
        '       <li id="test-wrap-2" class="menu-item">' +
        '       <a href="#horizontal-wrap-second">Tab 2</a>' +
        '       </li>' +
        '       <li class="menu-item">' +
        '           <a href="#horizontal-wrap-third">Tab 3 has a very long tab name</a>' +
        '       </li>' +
        '       <li class="menu-item">' +
        '           <a href="#horizontal-wrap-fourth">Tab4hasaveryverylongnonspacedname</a>' +
        '       </li>' +
        '   </ul>' +
        '   <div class="tabs-pane active-pane" id="horizontal-wrap-first-2">' +
        '        <h2>This is Tab 1</h2>' +
        '        <p>First</p>' +
        '   </div>' +
        '   <div class="tabs-pane" id="horizontal-wrap-second-2">' +
        '       <h2>This is Tab 2</h2>' +
        '       <p>Second</p>' +
        '   </div>' +
        '   <div class="tabs-pane" id="horizontal-wrap-third-2">' +
        '       <h2>This is Tab 3</h2>' +
        '       <p>Third</p>' +
        '   </div>' +
        '   <div class="tabs-pane" id="horizontal-wrap-fourth-2">' +
        '       <h2>This is Tab 4</h2>' +
        '       <p>Fourth</p>' +
        '   </div>' +
        '</div>';

    var $tabsContainer;
    var clock;

    beforeEach(function () {
        $('#test-fixture').append(responsiveTabs);
        $tabsContainer = $('#tabs-container');

        //Initially start off with full width
        $tabsContainer.css({
            width: getTotalVisibleTabsWidth() + 100
        });

        tabs.setup();

        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        $tabsContainer.remove();
        clock.restore();

        // This is so that multiple tabs.setup() calls don't conflict.
        $(window).off('resize');
    });

    function simulateResize(width) {
        $tabsContainer.css({
            width: width
        });

        $(window).trigger('resize');

        // Needs to be at least 200ms to get past the debounce.
        clock.tick(300);
    }

    function getVisibleTabs() {
        return $tabsContainer.find('.tabs-menu .menu-item');
    }

    function getVisibleTabWidths() {
        var $tabs = getVisibleTabs();
        var tabWidths = [];

        $tabs.each(function (i) {
            tabWidths.push($($tabs[i]).outerWidth());
        });

        return tabWidths;
    }

    function getTotalVisibleTabsWidth() {
        return getVisibleTabWidths().reduce(function (a, b) {
            return a + b;
        });
    }

    function countVisibleTabsAndDropdownTrigger() {
        return getVisibleTabs().length;
    }

    it('should use the immediate container width to calculate which tabs to show', function () {
        simulateResize(200);
        var totalTabWidth = getTotalVisibleTabsWidth();
        expect(totalTabWidth).to.be.below($tabsContainer.width());
    });

    it('when resized to fit two and a half items, two items should be left on the tab menu', function () {
        var tabWidths = getVisibleTabWidths();
        var resizedWidth = tabWidths[0] + tabWidths[1] + tabWidths[2] / 2;

        simulateResize(resizedWidth);

        expect(countVisibleTabsAndDropdownTrigger()).to.equal(3);
    });

    it('when resized down and resized back, the amount of tabs should be the same', function () {
        var tabWidths = getVisibleTabWidths();
        var originalWidth = $tabsContainer.outerWidth();
        var originalAmountOfTabs = countVisibleTabsAndDropdownTrigger();
        var resizedWidth = tabWidths[0] + tabWidths[1] + tabWidths[2] / 2;

        simulateResize(resizedWidth);
        simulateResize(originalWidth);

        expect(originalAmountOfTabs).to.equal(countVisibleTabsAndDropdownTrigger());
    });
});
