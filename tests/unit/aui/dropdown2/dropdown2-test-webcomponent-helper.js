import skate from 'skatejs';
import '../../../../src/js/aui/dropdown2';


var template = function (idPrefix) {
    function makeId(id) {
        return 'id="' + idPrefix + '-' + id + '"';
    }

    return {
        plainSection:
            '<aui-section ' + makeId('section1') + '>' +
            '   <aui-item-link href="#link" ' + makeId('item1') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item2') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item3') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item4') + '>Menu item</aui-item-link>' +
            '</aui-section>',

        plainSection2:
            '<aui-section ' + makeId('section2') + '>' +
            '   <aui-item-link ' + makeId('item12') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item22') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item32') + '>Menu item</aui-item-link>' +
            '   <aui-item-link ' + makeId('item42') + '>Menu item</aui-item-link>' +
            '</aui-section>',

        hiddenSection:
            '<aui-section ' + makeId('hidden-section') + '>' +
            '   <aui-item-checkbox hidden interactive disabled ' + makeId('hidden1-unchecked-disabled') + '>Menu item</aui-item-checkbox>' +
            '   <aui-item-checkbox hidden interactive checked ' + makeId('hidden2-checked') + '>Menu item</aui-item-checkbox>' +
            '</aui-section>',

        interactiveSection:
            '<aui-section ' + makeId('interactive-section') + '>' +
            '   <aui-item-radio interactive checked ' + makeId('iradio1-interactive-checked') + '>Menu item</aui-item-radio>' +
            '   <aui-item-radio interactive ' + makeId('iradio2-interactive-unchecked') + '>Menu item</aui-item-radio>' +
            '   <aui-item-radio ' + makeId('iradio3-unchecked') + '>Menu item</aui-item-radio>' +
            '</aui-section>',

        radioSection:
            '<aui-section ' + makeId('radio-section') + '>' +
            '   <aui-item-radio interactive  ' + makeId('radio1-unchecked') + '>Menu item</aui-item-radio>' +
            '   <aui-item-radio interactive checked ' + makeId('radio2-checked') + '>Menu item</aui-item-radio>' +
            '   <aui-item-radio interactive ' + makeId('radio3-unchecked') + '>Menu item</aui-item-radio>' +
            '</aui-section>',

        checkboxSection:
            '<aui-section ' + makeId('checkbox-section') + '>' +
            '   <aui-item-checkbox interactive  ' + makeId('check1-unchecked') + '>Menu item</aui-item-checkbox>' +
            '   <aui-item-checkbox interactive checked ' + makeId('check2-checked') + '>Menu item</aui-item-checkbox>' +
            '   <aui-item-checkbox interactive ' + makeId('check3-unchecked') + '>Menu item</aui-item-checkbox>' +
            '</aui-section>',

        submenuSection:
            '<aui-section ' + makeId('submenu-section') + '>' +
            '   <aui-item-link ' + makeId('dd2-menu-1-child-1') + '>Dummy item 1</aui-item-link>' +
            '   <aui-item-link for="#">Open submenu level 1</aui-item-submenu>' +
            '</aui-section>'
    };
};

var WebComponentDropdown = function () {
    var idPrefix = AJS.id('test-webcomponent-dropdown');
    var $dropdown = AJS.$('<aui-dropdown-menu id="' + idPrefix + '-menu"></aui-dropdown-menu>');

    var testComponent = {
        getItem: function (index, section) {
            var $itemContainer = section ? $dropdown.find('aui-section').eq(section - 1) : $dropdown;
            return $itemContainer.find('aui-item-link a, aui-item-checkbox span, aui-item-radio span').eq(index - 1);
        },

        addTrigger: function () {
            this.$trigger = AJS.$('<a id="' + idPrefix + '-trigger" href="#dropdown2-trigger" aria-owns="' + idPrefix + '-menu" aria-haspopup="true" class="aui-dropdown2-trigger">Example dropdown</a>');
        },

        addSecondTrigger: function () {
            this.$secondTrigger = AJS.$('<a id="' + idPrefix + '-second-trigger" href="#' + idPrefix + '-menu" aria-owns="' + idPrefix + '-menu" aria-haspopup="true" class="aui-dropdown2-trigger">Example dropdown(second trigger)</a>');
        },

        $dropdown: $dropdown,

        initialise: function ($parent) {
            this.$dropdown.appendTo($parent || AJS.$('#test-fixture'));
            skate.init(this.$dropdown[0]);

            if (this.$secondTrigger && this.$secondTrigger.length) {
                this.$secondTrigger.appendTo($parent || AJS.$('#test-fixture'));
                skate.init(this.$secondTrigger[0]);
            }
            if (this.$trigger && this.$trigger.length) {
                this.$trigger.appendTo($parent || AJS.$('#test-fixture'));
                skate.init(this.$trigger[0]);
            }
        },

        addPlainSection: function () {
            $('#' + idPrefix + '-section1').remove();
            $dropdown.append(
                template(idPrefix).plainSection
            );
        },

        addPlainSection2: function () {
            $('#' + idPrefix + '-section2').remove();
            $dropdown.append(
                template(idPrefix).plainSection2
            );
        },

        addHiddenSection: function () {
            $('#' + idPrefix + '-section3').remove();
            $dropdown.append(
                template(idPrefix).hiddenSection
            );
        },

        addInteractiveSection: function () {
            $('#' + idPrefix + '-section4').remove();
            $dropdown.append(
                template(idPrefix).interactiveSection
            );
        },

        addRadioSection: function () {
            $('#' + idPrefix + '-section2').remove();
            $dropdown.append(
                template(idPrefix).radioSection
            );
        },

        addCheckboxSection: function () {
            $('#' + idPrefix + '-checkbox-section').remove();
            $dropdown.append(
                template(idPrefix).checkboxSection
            );
        },

        addSubmenuSection: function (dropdownToAssociate) {
            var $submenuSection = $(template(idPrefix).submenuSection);

            $submenuSection.find('aui-item-link[for]').attr('for', dropdownToAssociate.$dropdown[0].id);

            $dropdown.append($submenuSection);
        },

        checkEvent: 'aui-dropdown2-item-check',
        uncheckEvent: 'aui-dropdown2-item-uncheck',

        isChecked: function (el) {
            var $el = $(el);
            el = $el[0];

            var parent;
            var child;

            var tagName = el.tagName.toLowerCase();
            if (tagName === 'aui-item-checkbox' || tagName === 'aui-item-radio') {
                parent = el;
                child = el.firstChild;
            } else {
                parent = el.parentNode;
                child = el;
            }

            return $(child).is('[aria-checked="true"].checked.aui-dropdown2-checked') && parent.hasAttribute('checked');
        },

        isUnchecked: function (el) {
            var $el = $(el);
            el = $el[0];

            var parent;
            var child;

            var tagName = el.tagName.toLowerCase();
            if (tagName === 'aui-item-checkbox' || tagName === 'aui-item-radio') {
                parent = el;
                child = el.firstChild;
            } else {
                parent = el.parentNode;
                child = el;
            }

            return !$(child).is('.checked, .aui-dropdown2-checked') && $(child).is('[aria-checked="false"]') && !parent.hasAttribute('checked');
        }

    };

    return testComponent;
};

export default WebComponentDropdown;
