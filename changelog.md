# 6.0.2
* [Documentation](https://docs.atlassian.com/aui/6.0.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.2)

## Fixed
* Tooltips with overflowing content would sometimes overflow over the triggering component.

# 6.0.1
* [Documentation](https://docs.atlassian.com/aui/6.0.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.1)

## Fixed
* Repeated initialisation of keyboard shortcuts no longer duplicates text in title attributes.
* WhenIType now uses native events instead of jQuery events. This works better with Synthetic Events in React.
* Responsive (asnyc) header submenus now display when the header is initialised at a narrow screen width.

# 6.0.0
* [Documentation](https://docs.atlassian.com/aui/6.0.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=6.0.0)

## Upgrade Notes
* The Raphael (`com.atlassian.auiplugin:ajs-raphael`) and Eve (`com.atlassian.auiplugin:ajs-evejs`) resources have been removed.
* The legacy Inline Dialog component had two deprecated methods removed which used Raphael to draw arrows/shadows.
* The draw-logo script was also removed from the legacy AJS resource.

## Removed
* Removed the Raphael and Eve web resources.
* Removed the `getArrowAttributes` and `getArrowPath` methods from the legacy Inline Dialog component.
* Removed the draw-logo resource used in the legacy AJS resource.

# 5.10.1
* [Documentation](https://docs.atlassian.com/aui/5.10.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.10.1)

## Added
* Header can now be loaded asynchronously by including the `aui-header-async` resource.

## Removed
* AUI was erroneously exposing an `aui-header` test web component. This has been removed.

# 5.10.0
* [Documentation](https://docs.atlassian.com/aui/5.10.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.10.0)

## Upgrade Notes
* This release is broken. Please use 5.10.1 instead.

# 5.9.24
[Documentation](https://docs.atlassian.com/aui/5.9.24/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.24)

## Fixed
* Validator again exposes AJS.I18n and AJS.format

# 5.9.23
[Documentation](https://docs.atlassian.com/aui/5.9.23/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.23)

## Fixed
* Date validator now correctly handles dates in October
* Added missing 'aui-dropdown2-in-header' and 'aui-dropdown2-in-buttons' classes to the dropdown menu when trigger is inside a header or buttons container.

# 5.9.22
[Documentation](https://docs.atlassian.com/aui/5.9.22/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.22)

## Fixed
* Using the keyboard to follow a dropdown item href link now works when the dropdown is first opened.
* Text for menu items in a collapsed sidebar now renders correctly on screen readers.

## Changed
* Improved documentation content and structure.

# 5.9.21
[Documentation](https://docs.atlassian.com/aui/5.9.21/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.21)

# 5.9.20
[Documentation](https://docs.atlassian.com/aui/5.9.20/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.20)

## Fixed
* Fixed broken package dependency which was causing installs to fail.
* Fixed issue where related aui-toggle property/attribute values did not match in Microsoft Edge.
* Fixed issue when initially-open Expander component could not be closed

## Changed
* Upgraded Skate to 0.13.17 to get fixes related to Microsoft Edge.
* Improved documentation content and structure.

# 5.9.19
[Documentation](https://docs.atlassian.com/aui/5.9.19/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.19)

## Fixed
* Fixed the dialog arrow being positioned incorrectly when a deprecated Inline Dialog is instantiated with special CSS characters in the identifier.

# 5.9.18
[Documentation](https://docs.atlassian.com/aui/5.9.18/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.18)

## Fixed
* Fixed Dropdown2 always changing the `href` attribute of its trigger to `'#'` even if the attribute was valid.
* Fixed inconsistencies between keyboard and mouse inputs when interacting with Dropdown2 menus with attached event handlers.
* Calling AJS.dim multiple times no longer resets its known list of aria-hidden elements.
* Fixed: aui-select component now correctly retains selected value on IE11
* Dropdown menus now support multiple triggers.
* Children of Inline dialog no longer lose event handlers during templating in non Chrome browsers
* Fixed: The aui-inline-dialog 'open' property and attribute now return consistent values
* Fixed Expander trigger text changing when an `aui-expander-expand` or `aui-expander-collapse` event is triggered and the expander is already expanded or collapsed.

# 5.9.17
[Documentation](https://docs.atlassian.com/aui/5.9.17/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.17)

## Fixed
* Submenus in the sidebar can now be opened by calling the `show` method.
* Opening and closing a Dialog2 no longer overrides existing overflow styles

## Added
* Added a new event ("aui-responsive-menu-link-created") for when menus get moved into the responsive header.

# 5.9.16
[Documentation](https://docs.atlassian.com/aui/5.9.16/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.16)

## Fixed
* Fixed a bug with Inline Dialog where an inline dialog with `responds-to="hover"` could fail to display correctly.
* Fixed an issue where a component anchored to another element (such as Dropdown 2) would duplicate alignment classes.
* Sidebar's submenus are initialised lazily, so adding submenus after calling AJS.sidebar(el) will work. This is the same behaviour as 5.7.x had.

# 5.9.15
[Documentation](https://docs.atlassian.com/aui/5.9.15/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.15)

# 5.9.14
[Documentation](https://docs.atlassian.com/aui/5.9.14/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.14)

# 5.9.13
[Documentation](https://docs.atlassian.com/aui/5.9.13/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.13)

## Changed
* Upgraded Skate to 0.13.16. Skate no longer shares an element registry with older versions. This was causing problems with the lifecycles being initialised multiple times.

## Fixed
* Fixed an issue where flags had incorrect CSS for its borders

# 5.9.12
[Documentation](https://docs.atlassian.com/aui/5.9.12/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.12)

## Fixed
* Back port from master of fix for event handler leakage in the AUI sidebar
* Back port from master of fix for form attribute handler for toggle button that wouldn't work in IE

# 5.9.11
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.10
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.9
## Upgrade Notes
* This release is broken. Please use 5.9.12 instead.

# 5.9.8
[Documentation](https://docs.atlassian.com/aui/5.9.8/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.8)

## Fixed
* Fixed alignment of the dialog arrow for Inline Dialog 2.

## Removed
* AUI was adding deprecated classes when `AJS.messages` was being called. This has since been removed. `AJS.messages.warning` will add `.aui-message-warning` only, not `.warning`.

# 5.9.7
[Documentation](https://docs.atlassian.com/aui/5.9.7/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.7)

## Fixed
* Fixed an issue where setting the `hidden` property for an object in the JSON response for an async dropdown would not set the attribute on the item.
* Patched jquery.ui.mouse.js for AUI edge compatibility (backported https://bugs.jqueryui.com/ticket/7778 for v1.8.24)

# 5.9.6
[Documentation](https://docs.atlassian.com/aui/5.9.6/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.6)

# 5.9.5
[Documentation](https://docs.atlassian.com/aui/5.9.5/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.5)

## Fixed
* Fixed an issue where specifying a value for an `<aui-option>` inside an `<aui-select>` caused the option to not get properly selected.
* i18n fixed for `<aui-toggle>`, `<aui-select>` and `<aui-dropdown>`

## Added
* Event handlers attached to a sidebar can now be removed with the `off()` method.

# 5.9.4
[Documentation](https://docs.atlassian.com/aui/5.9.4/)
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.4)

## Fixed
* Sidebar collapse/expand shortcut ([) no longer conflicts with browser back shortcut (CMD+[) on Mac OS X.

# 5.9.3
* [Documentation](https://docs.atlassian.com/aui/5.9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.3)

## Fixed
* Events are now fired correctly when a progress indicator is updated.
* Tooltips using the `live` option now work correctly when initialised on empty collections.

# 5.9.2
* [Documentation](https://docs.atlassian.com/aui/5.9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.2)

## Added
* Updated Soy templates to allow classes to be specified for form field elements directly using the `extraFieldClasses` parameter.

# 5.9.1
* [Documentation](https://docs.atlassian.com/aui/5.9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.1)

## Added
* Tipsy tooltips can be destroyed with `$el.tipsy('destroy')`.

## Changed
* AUI datepicker internationalisation files have been changed from `.properties` files to `.js` files, for more modular consumption in plugins.
* An ID given to an <aui-select> is no longer moved to the <input> of the expanded DOM. Instead it behaves in line with the <aui-toggle>, adding the given ID with a suffix '-input' to the <input> of the expanded DOM.
* (perf) Removed fraction support for jQuery < 1.5
* (cosmetic) Min- and maxlength error messages for form validation are now more precise
* (cosmetic) Form validation message adapts when minlength == maxlength

## Fixed
* Dropdown, Dropdown 2, and Single Select no longer have a FOUC.
* Toggle Button's `checked` attribute now properly stays in sync with its state.
* Clicking a child of a dropdown item with the `aui-dropdown2-interactive` class doesn't hide the dropdown any more
* Tooltips are positioned correctly on `<svg>` elements, and their children.
* Expose named defines to make AUI work with AlmondJS
* Allow click event on a checkbox or radio button in a dropdown to be prevented

# 5.9.0
* [Documentation](https://docs.atlassian.com/aui/5.9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.0)
* [Upgrade guide](https://docs.atlassian.com/aui/5.9.0/docs/upgrade-guide.html)

## Added
* AUI is now available on the public npm registry. To install: `npm install @atlassian/aui`
* `can-create-values` boolean attribute for `<aui-select>` that allows the user to create new values.
* `no-empty-option` boolean attribute for `<aui-select>` that disallows empty choices.
* Experimental `<aui-dropdown-menu>` web component API for Dropdown 2.
* `<aui-toggle>` web component for toggle buttons ("on" and "off" states).
* `<aui-label>` web component that serves as a complement to specific AUI components, e.g., `<aui-toggle>`, to provide an accessible label.
* `<aui-badge>` web component API for Badges.
* Moved docs and test pages from AUI-ADG to AUI.

## Changed
* Refactored build from grunt to gulp.
* Experimental components no longer need a `require()` call to load them.
* `<aui-inline-dialog2>` renamed to `<aui-inline-dialog>`.
* `<aui-inline-dialog>` no longer needs a `require()` call to load it.
* `<aui-inline-dialog>`'s `persistent` attribute is now a boolean attribute.

## Deprecated
* All usage of named AMD modules. Use UMD source instead.
* `AJS()`, `AJS.filterBySearch()`, `AJS.include()`, `AJS.setVisible()`, `AJS.setCurrent()`, and `AJS.isVisible()` have been deprecated. Use jQuery or native alternatives instead.
* RESTful table helper methods (`AJS.triggerEvt()`, `AJS.bindEvt()`, and `AJS.triggerEvtForInst()`) have been deprecated and moved to RESTful table.
* Accessing the jQuery element on form validation fields via `field.$el` has been deprecated. Use `field.el` instead, which contains the native DOM element.
* `.aui-badge` class-based API for Badges has been deprecated. Use the `<aui-badge>` web component API instead.

## Fixed
* Several theming fixes and enhancements.
* Dropdowns are now closed when the browser window is resized.
* Sortable table arrow directions have been flipped to reflect the actual ordering.

## Removed
* `<aui-inline-dialog>`'s `isVisible()`, `show()`, and `hide()` have been removed. Use the `open` property instead.
* `<aui-inline-dialog>`'s `aui-layer-show` and `aui-layer-hide` events have been removed. Use `aui-show` and `aui-hide` instead.

# 5.8.16
* [Documentation](https://docs.atlassian.com/aui/5.8.16/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.16)

# 5.8.17
* [Documentation](https://docs.atlassian.com/aui/5.8.17/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.17)

## Changed
* Converted date picker to use properties files instead of hardcoded values.

## Fixed
* Fixed focus not being trapped inside a modal dialog for screen reader users.
* Tooltips are positioned correctly on `<svg>` elements, and their children.

## Added
* Tipsy tooltips can be destroyed with `$el.tipsy('destroy')`.

# 5.8.15
* [Documentation](https://docs.atlassian.com/aui/5.8.15/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.15)

## Fixed
* Web components, e.g., Inline Dialog 2, are no longer subject to premature initialization in IE9 and IE10 that caused them to break.
* Dropdown 2 no longer has a resizing bug when open where it would stretch to the height of the browser when the browser was resized.
* Inline Dialog 2 has `display` set to `none` after hiding so that it no longer takes up space when hidden.
* Inline Dialog 2 no longer has a FOUC.
* Inline Dialog 2 now correctly fades when hidden.
* Inline Dialog 2 now checks if the show and/or hide event was cancelled before updating its trigger's `aria-expanded` attribute.
* Sidebar correctly unbinds keyboard handler for '[' shortcut, fixing a regression introduced in 5.8.14.
* Single Select's value can now be programatically set via the element's `value` property.
* Single Select now shows the display name of an option, rather than the text of the `value` attribute, fixing a regression introduced in 5.8.14.

# 5.8.14
* [Documentation](https://docs.atlassian.com/aui/5.8.14/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.14)

## Fixed
* Sidebar now only enters touch mode for small screen touchable devices, i.e., width < 1024px.
* `value` property of `<aui-select>` now correctly returns the selected option's value rather than its label.
* Stopped jQuery Hotkeys throwing an error about `options.combo` being undefined.
* Sidebar no longer toggles when '[' is typed in an input field.

# 5.8.13
* [Documentation](https://docs.atlassian.com/aui/5.8.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.13)

## Added
* Added `debounceImmediate()` (similar to `debounce()` but is immediately invoked).

## Changed
* Backport of jQuery UI feature detection performance improvement.
* "More" menu for responsive header is now lazily created to reduce work in onReady.
* Removed base64-encoded data URIs for icons from sidebar CSS. Changed to reference actual icon files now.

## Removed
* removed CSS vendor prefixes for `box-sizing` and `transition`

## Fixed
* `AJS.version` now contains the correct value rather than `${project.version}` (broken since 5.8.11).
* Various bug fixes for responsive header (reinsertion order, disappearing items, quickly resizing, keyboard navigation).
* Fixed a JavaScript error in the responsive header when secondary navigation was hidden.
* Fixed navigation soy template when there are collapsible children.
* Fixed keyboard navigation bug in Sidebar around collapsible children.

# 5.8.12
* [Documentation](https://docs.atlassian.com/aui/5.8.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.12)

## Changed
* Clicking on empty space in sidebar no longer collapses or expands it.

## Fixed
* Improved robustness of Raphael detection.
* Sidebar in documents with short content now properly reflows when expanding window size in Chrome.
* Sidebar submenus can now be focused with the keyboard when collapsed.
* Keyboard shortcut ("[") for toggling the sidebar now works on German keyboard layouts.
* Fixed a sidebar JavaScript bug in IE9 (invalid classList lookup).
* Fixed a sidebar bug in IE9/10 where submenus would incorrectly show when hovered over.

# 5.8.11
* [Documentation](https://docs.atlassian.com/aui/5.8.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.11)

## Changed
* dropdown2's `.aui-dropdown2-checkbox` and `.aui-dropdown2-radio` have had their `isDisabled()` method replaced with `isEnabled()`.

## Fixed
* Responsive header dropdown2s now properly open as submenus.
* Hidden dropdown2s no longer break alignment of menus on window resize.

# 5.8.10
* [Documentation](https://docs.atlassian.com/aui/5.8.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.10)

## Fixed
* `eve.unbind()` is once again aliased as `eve.off()`.

# 5.8.9
* [Documentation](https://docs.atlassian.com/aui/5.8.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.9)

## Fixed
* Proper fix for messages wrapping long unbroken strings.

# 5.8.8
* [Documentation](https://docs.atlassian.com/aui/5.8.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.8)

## Fixed
* Form validation attributes changed at runtime are now respected on subsequent field validations.
* Messages now wrap long unbroken strings (break-word).
* Links (<a>) in the expanded sidebar now work again (broken since 5.8.0).
* Sidebar correctly sizes and positions itself at page load for short content heights.

# 5.8.7
* [Documentation](https://docs.atlassian.com/aui/5.8.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.7)

## Highlights
* Sidebar events now support `preventDefault()`.

# 5.8.6
* [Documentation](https://docs.atlassian.com/aui/5.8.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.6)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.5
* [Documentation](https://docs.atlassian.com/aui/5.8.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.5)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.4
* [Documentation](https://docs.atlassian.com/aui/5.8.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.4)

# 5.8.3
* [Documentation](https://docs.atlassian.com/aui/5.8.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.3)

# 5.8.2
* [Documentation](https://docs.atlassian.com/aui/5.8.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.2)

## Upgrade Notes
* This release has some missing artifacts, please use 5.8.3 instead.

# 5.8.1
* [Documentation](https://docs.atlassian.com/aui/5.8.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.1)

# 5.8.0
[Documentation](https://docs.atlassian.com/aui/5.8.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.0)

## Highlights
* NEW COMPONENT: [AUI Single Select](http://docs.atlassian.com/aui/5.8.0/single-select.html)
* Dropdown2 has been rewritten to improve accessibility. There is a new markup pattern, detailed at [http://docs.atlassian.com/aui/5.8.0/dropdown2.html](http://docs.atlassian.com/aui/5.8.0/dropdown2.html). While the old markup pattern will work backwards compatibly, we encourage all developers to move to the new markup pattern to improve access for people using screen readers.

## Upgrade Notes
* aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
* If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js
* The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents)
* The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.
* Removed dependencies from AUI components to AUI soy templates (including responsive header).
* AUI Sandbox has been removed, in the future we will be uploading examples to jsbin.
* AUI Flag `persistent` option has been removed (deprecated in 5.7.7).  Update all usages of this option to use `close` instead (see http://docs.atlassian.com/aui/5.8.0/flag.html).
* An AMD loader must be included in order to use aui-experimental.js now.

# 5.7.34
[Documentation](https://docs.atlassian.com/aui/5.7.34/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.34)

## Changed
* Converted date picker to use properties files instead of hardcoded values.

## Fixed
* Fixed focus not being trapped inside a modal dialog for screen reader users.

# 5.7.33
[Documentation](https://docs.atlassian.com/aui/5.7.33/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.33)

# 5.7.32
[Documentation](https://docs.atlassian.com/aui/5.7.32/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.32)

## Fixed
* Ensure Inline Dialog 2 has `display` set to `none` after hiding so that it doesn't take up space.

# 5.7.31
[Documentation](https://docs.atlassian.com/aui/5.7.31/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.31)

## Fixed
* Sidebar now only enters touch mode for small screen touchable devices, i.e., width < 1024px.
* Sidebar no longer toggles when '[' is typed in an input field.

# 5.7.30
[Documentation](https://docs.atlassian.com/aui/5.7.30/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.30)

# 5.7.29
[Documentation](https://docs.atlassian.com/aui/5.7.29/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.29)

## Added
* Added `debounceImmediate()` (similar to `debounce()` but is immediately invoked).

## Fixed
* Fixed a JavaScript error in the responsive header when secondary navigation was hidden.

# 5.7.28
[Documentation](https://docs.atlassian.com/aui/5.7.28/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.28)

## Changed
* Clicking on empty space in sidebar no longer collapses or expands it.
* Backport of jQuery UI feature detection performance improvement.
* "More" menu for responsive header is now lazily created to reduce work in onReady.

## Fixed
* Sidebar submenus can now be focused with the keyboard when collapsed.
* Various bug fixes for responsive header (reinsertion order, disappearing items, quickly resizing).

# 5.7.27
[Documentation](https://docs.atlassian.com/aui/5.7.27/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.27)

## Fixed
* Sidebar in documents with short content now properly reflows when expanding window size in Chrome.
* Improved performance of jQuery UI feature detection.

# 5.7.26
[Documentation](https://docs.atlassian.com/aui/5.7.26/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.26)

## Fixed
* `eve.unbind()` is once again aliased as `eve.off()`.
* Improved robustness of Raphael detection.

# 5.7.25
[Documentation](https://docs.atlassian.com/aui/5.7.25/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.25)

## Fixed
* Proper fix for messages wrapping long unbroken strings.

# 5.7.24
[Documentation](https://docs.atlassian.com/aui/5.7.24/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.24)

## Fixed
* Form validation attributes changed at runtime are now respected on subsequent field validations.
* Messages now wrap long unbroken strings (break-word).

# 5.7.23
[Documentation](https://docs.atlassian.com/aui/5.7.23/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.23)

# 5.7.22
[Documentation](https://docs.atlassian.com/aui/5.7.22/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.22)

# 5.7.21
[Documentation](https://docs.atlassian.com/aui/5.7.21/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.21)

* This is a broken release. Use 5.7.22 instead.

## Highlights
* Sidebar events now support `preventDefault()`.

# 5.7.20
[Documentation](https://docs.atlassian.com/aui/5.7.20/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.20)

# 5.7.19
[Documentation](https://docs.atlassian.com/aui/5.7.19/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.19)

# 5.7.18
[Documentation](https://docs.atlassian.com/aui/5.7.18/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.18)

# 5.7.17
[Documentation](https://docs.atlassian.com/aui/5.7.17/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.17)

# 5.7.16
[Documentation](https://docs.atlassian.com/aui/5.7.16/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.16)

## Highlights
* The dist is now using version 0.11.4 of grunt-less-contrib, and version ~1.7.2 of the less compiler.

# 5.7.15
[Documentation](https://docs.atlassian.com/aui/5.7.15/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.15)

# 5.7.14
[Documentation](https://docs.atlassian.com/aui/5.7.14/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.14)

# 5.7.13
[Documentation](https://docs.atlassian.com/aui/5.7.13/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.13)

# 5.7.12
[Documentation](https://docs.atlassian.com/aui/5.7.12/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.12)

# 5.7.11
[Documentation](https://docs.atlassian.com/aui/5.7.11/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.11)

# 5.7.10
[Documentation](https://docs.atlassian.com/aui/5.7.10/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.10)

# 5.7.9
[Documentation](https://docs.atlassian.com/aui/5.7.9/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.9)

# 5.7.8
[Documentation](https://docs.atlassian.com/aui/5.7.8/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.8)

# 5.7.7
[Documentation](https://docs.atlassian.com/aui/5.7.7/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.7)

## Highlights
* Fixed persistent flags to be dismissable. See [AUI-2893](https://ecosystem.atlassian.net/browse/AUI-2893)

## Upgrade Notes
**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Flags created with the previous 5.7.1 API will work exactly as they did previously in 5.7.7. Upgrading from these versions
is **non breaking** (existing calls to flag will behave exactly as they did). However, the "persistent" has been deprecated,
and the "close" option introduced instead.
* Flags with "persistent: true" should become "close: never"
* Flags with "persistent: false" should become "close: manual" (close: "manual" is the default, so you do not need to explicitly set this)

**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Upgrading from 5.7.4 **will change** behaviour of flags with "persistent: false" set. In 5.7.4, flags automatically faded
from view. They now require a user to dismiss them (they have close: "manual" behaviour)
* Flags with "persistent: true" should become "close: never". Flags with "persistent: true" maintain the same behaviour.
* Flags with "persistent: false" no longer fade after five seconds. If you wish them to, they should become "close: auto". If you do do not wish them to, they should become "close: manual"

#5.7.6
This is a broken release containing no bugfixes and should not be used. Please use the latest 5.7.x release instead

#5.7.5
[Documentation](https://docs.atlassian.com/aui/5.7.5/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.5)

#5.7.4
[Documentation](https://docs.atlassian.com/aui/5.7.4/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.4)

## Highlights
* Flags auto-close was ported over to this release from master.

#5.7.3
[Documentation](https://docs.atlassian.com/aui/5.7.3/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.3)

## Highlights
* New Confluence Icon Fonts
Note: normally we would do this in a minor version. However to avoid upgrade friction for Confluence we've determined it is acceptable to put these in a patch release.

#5.7.2
[Documentation](https://docs.atlassian.com/aui/5.7.2/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.2)

#5.7.1
[Documentation](https://docs.atlassian.com/aui/5.7.1/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.1)

#5.7.0
[Documentation](https://docs.atlassian.com/aui/5.7.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.0)

## Highlights
* Buttons have become flat and have had gradients removed
* Inset shadows have been removed from fields in forms
* Form notification is available as a new API (show tooltip messages on fields)
* Flag messages are available as a new API
* Inline Dialog 2 is available as a new API
* System notifications have been changed (no longer electric Charlie)
* The following components have no global variable and are only exposed via AMD modules:
  * Flags
  * Form Notification
  * Form Validation
  * Inline Dialog 2
* [Animated examples](https://developer.atlassian.com/display/AUI/AUI+5.7.0+Release+Notes) are available.

## Upgrade Notes
* If you have been using Form Validation, please change your data attributes
  * from: `<input data-aui-validate-... />`
  * to: `<input data-aui-validation-... />`
* If you are using Inline Dialog 2, Flags, Form Notification or Form Validation, you will need an AMD loader to initialise their behaviour on pages:
  * `require(['aui/inline-dialog2']); // Initialises all Inline Dialog 2s`
  * `require(['aui/flag']); // Initialises all flags`
  * `require(['aui/form-notification']); // Initialises all form notifications`
  * `require(['aui/form-validation', 'form-provided-validators']); // Loads and initialises form validations (and notifications) API. Also loads all AUI-provided form validators`

For all release notes older than 5.7.0, check [DAC](https://developer.atlassian.com/display/AUI/AUI+release+notes)
