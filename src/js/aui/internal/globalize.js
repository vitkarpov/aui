'use strict';

import createElement from '../create-element';
import objectAssign from 'object-assign';

var NAMESPACE = 'AJS';

export default function (name, value) {
    window[NAMESPACE] = objectAssign(createElement, window[NAMESPACE]);

    return window[NAMESPACE][name] = value;
}
