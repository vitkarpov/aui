'use strict';

import $ from '../../../src/js/aui/jquery';
import params from '../../../src/js/aui/params';
import populateParameters from '../../../src/js/aui/populate-parameters';

describe('aui/populate-parameters', function () {
    it('globals', function () {
        expect(AJS.params).to.equal(params);
        expect(AJS.populateParameters).to.equal(populateParameters);
    });

    describe('API', function () {
        afterEach(function () {
            Object.keys(params).forEach(key => delete params[key]);
        });

        it('with list input', function () {
            $('<fieldset class="parameters"><input title="test1" class="list" value="value1"><input title="test1" class="list" value="value2"></fieldset>').appendTo('#test-fixture');
            populateParameters();
            expect(params.test1.length).to.equal(2);
            expect(params.test1[0]).to.equal('value1');
            expect(params.test1[1]).to.equal('value2');
        });

        it('with no parameter', function () {
            $('<fieldset class="parameters"><input id="test1" value="value1"></fieldset>').appendTo('#test-fixture');
            populateParameters();
            expect(params.test1).to.equal('value1');
        });

        it('with parameter', function () {
            $('<fieldset class="parameters"><input id="test1" value="value1"></fieldset>').appendTo('#test-fixture');
            var toPopulate = {};
            populateParameters(toPopulate);
            expect(toPopulate.test1).to.equal('value1');
            expect(params.test1).to.equal(undefined);
        });
    });
});
