# AUI - Atlassian UI

The Atlassian User Interface library.

## Installation

Install via npm:

`npm install @atlassian/aui`

In the npm package:

  * `dist/` contains pre-compiled javascript and css. This is the simplest way to use AUI.

  * `lib/` contains individual AUI components. Use a CommonJS or AMD loader to load each component as your application needs.

  * `src/` contains the es6 (babel) and LESS sources. It's unlikely you'll require these directly.

Install via bower:

`bower install https://bitbucket.org/atlassian/aui-dist.git`

Or [use our CDN](http://aui-cdn.atlassian.com/).

## Documentation

Thorough documentation is available at [the AUI website](https://docs.atlassian.com/aui/latest/).


## Developing AUI

### Requirements

- Java 1.7 - for building the soy templates.
- Node 0.12+
- npm

### Building

`npm install` takes care of everything for you.

    npm install

To build the distribution:

    npm run dist

To build the UMD files:

    npm run umd

### Unit tests

Although we encourage you write your tests first, then your code, you can test your changes in a couple areas.

We use [Karma](http://karma-runner.github.io/0.10/index.html) for running our unit tests.

To run tests once:

    npm test

We use [Karma](http://karma-runner.github.io) for running our tests.

To run the tests in watch mode:

    npm run test/watch

Both `test` and `run test/watch` accept the same arguments.

#### Arguments

`--browsers [Chrome,Firefox]`

The browsers to run the tests in. Valid values are `Chrome`, `Firefox`, `Opera` and `Safari`.

`--grep [pattern]`

Specify a pattern to match a subset of tests to run.

*It's recommended to just run a single describe block in TDD mode, use [`describe.only`](https://mochajs.org/#exclusive-tests)*

#### Unit tests via Docker

Alternatively, you can run the Chrome and Firefox unit tests via `npm run test-docker/chrome` or `npm run test-docker/firefox`.

### Visual tests

We have a reference application called the `flatapp`. We use this to visualise our changes during development and testing.

To build the flatapp and run a server:

    npm run flatapp

To watch for changes in source files and automatically update the files being served:

    npm run flatapp/watch

#### Arguments

`--host [0.0.0.0]`

The host to start the server on.

`--port [7000]`

The port to start the server on.

*By default this will open up a page at [http://0.0.0.0:7000/pages/](http://0.0.0.0:7000/pages/).*

## Git hooks

To set up git hooks run `npm run setuphooks`.

## Documenting

To build the docs and run a server:

    npm run docs

To watch for changes in source files and automatically update the files being served:

    npm run docs/watch

### Arguments

`--host [0.0.0.0]`

The host to start the server on.

`--port [8000]`

The port to start the server on.

*By default this will open up a page at [http://0.0.0.0:8000/](http://0.0.0.0:8000/).*

## CLI

To see a list of the commands:

    npm run

To see the help for a specific command pass `--help` to it.

## How do you get it?

AUI distributions are released to the [aui-dist repo on Bitbucket](https://bitbucket.org/atlassian/aui-dist).

##Additional documentation

* [Component documentation](https://docs.atlassian.com/aui/latest/)
* [Changelog](https://bitbucket.org/atlassian/aui/src/master/changelog.md?at=master)

## Raising issues

Raise bugs or feature requests in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

## Contributing

Contributions to AUI are via pull request.

- Create an issue in the [AUI project](https://ecosystem.atlassian.net/browse/AUI). Creating an issue is a good place to
talk the AUI team about whether anyone else is working on the same issue, what the best fix is, and if this is a new feature,
whether it belongs in AUI. If you don't create an issue, we'll ask you to create one when you issue the PR and retag your
commits with the issue key.
- If you have write access to the AUI repo (ie if you work at Atlassian), you can create branches in the main AUI repo -
name your branch as `{issue-key}-{description}`, eg `AUI-1337-fix-the-contributor-guide`. If you don't have
write access, please fork AUI and issue a PR.
- Ensure all commits are tagged with the issue key (`AUI-1337 fixes to contributor guide`).
- Write tests. Unit tests are preferred over integration tests.
- Most PRs will go into master, however you might want them to go into a stable branch. If so, set the target branch
as the stable branch and the AUI team will manage merging stable into master after the PR is through.

## Compatibility

AUI supports the following browsers:

- Chrome latest stable
- Firefox latest stable
- Safari latest stable (on OS X only)
- IE 9+

## License

AUI is released under the [Apache 2 license](https://bitbucket.org/atlassian/aui/src/master/LICENSE).
See the [licenses directory](https://bitbucket.org/atlassian/aui/src/master/licenses/) for information about AUI and included libraries.
