'use strict';

import $ from './jquery';
import createElement from './create-element';
import globalize from './internal/globalize';

function onTextResize (f) {
    if (typeof f === 'function') {
        if (onTextResize['on-text-resize']) {
            onTextResize['on-text-resize'].push(function (emsize) {
                f(emsize);
            });
        } else {
            var em = createElement('div');

            em.css({
                width: '1em',
                height: '1em',
                position: 'absolute',
                top: '-9999em',
                left: '-9999em'
            });

            $('body').append(em);
            em.size = em.width();

            setInterval(function () {
                if (em.size !== em.width()) {
                    em.size = em.width();

                    for (var i = 0, ii = onTextResize['on-text-resize'].length; i < ii; i++) {
                        onTextResize['on-text-resize'][i](em.size);
                    }
                }
            }, 0);
            onTextResize.em = em;
            onTextResize['on-text-resize'] = [function (emsize) {
                f(emsize);
            }];
        }
    }
}

globalize('onTextResize', onTextResize);

export default onTextResize;
